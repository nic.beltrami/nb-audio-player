import {Injectable} from '@angular/core';
import {Song} from '../models/song';
import {Observable, of} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {plainToClass} from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  constructor() {
  }

  public findAll(): Observable<Song[]> {
    const json = [
      {
        title: 'HAYDN "CONCERTO D-MAJOR"',
        artist: 'Reiner Hockmuth',
        album: 'Cellist',
        audioUrl: 'http://www.hochmuth.com/mp3/Haydn_Cello_Concerto_D-1.mp3',
        albumCoverUrl: 'http://www.hochmuth.com/images/picture2small.jpg'
      },
      {
        title: 'TCHAIKOVSKY "ROCOCO-VARIATIONS"',
        artist: 'Reiner Hockmuth',
        album: 'Cellist',
        audioUrl: 'http://www.hochmuth.com/mp3/Tchaikovsky_Rococo_Var_orch.mp3',
        albumCoverUrl: 'http://www.hochmuth.com/images/picture2small.jpg'
      },
      {
        title: 'VIVALDI SONATA "ADAGIO"',
        artist: 'Reiner Hockmuth',
        album: 'Cellist',
        audioUrl: 'http://www.hochmuth.com/mp3/Vivaldi_Sonata_eminor_.mp3',
        albumCoverUrl: 'http://www.hochmuth.com/images/picture2small.jpg'
      },
      {
        title: 'TCHAIKOVSKY "NOCTURNE"',
        artist: 'Reiner Hockmuth',
        album: 'Cellist',
        audioUrl: 'http://www.hochmuth.com/mp3/Tchaikovsky_Nocturne__orch.mp3',
        albumCoverUrl: 'http://www.hochmuth.com/images/picture2small.jpg'
      },
      {
        title: 'HAYDN "ADAGIO"',
        artist: 'Reiner Hockmuth',
        album: 'Cellist',
        audioUrl: 'http://www.hochmuth.com/mp3/Haydn_Adagio.mp3',
        albumCoverUrl: 'http://www.hochmuth.com/images/picture2small.jpg'
      },
      {
        title: 'BOCCHERINI "CONCERTO IN D"',
        artist: 'Reiner Hockmuth',
        album: 'Cellist',
        audioUrl: 'http://www.hochmuth.com/mp3/Boccherini_Concerto_478-1.mp3',
        albumCoverUrl: 'http://www.hochmuth.com/images/picture2small.jpg'
      },
      {
        title: 'BLOCH "PRAYER"',
        artist: 'Reiner Hockmuth',
        album: 'Cellist',
        audioUrl: 'http://www.hochmuth.com/mp3/Bloch_Prayer.mp3',
        albumCoverUrl: 'http://www.hochmuth.com/images/picture2small.jpg'
      },
      {
        title: 'BEETHOVEN "VARIATIONS"',
        artist: 'Reiner Hockmuth',
        album: 'Cellist',
        audioUrl: 'http://www.hochmuth.com/mp3/Beethoven_12_Variation.mp3',
        albumCoverUrl: 'http://www.hochmuth.com/images/picture2small.jpg'
      },
    ];
    return of(json).pipe(
      map(songs => plainToClass(Song, songs)),
      tap(songs => {
        songs.forEach(song => {
          if (!song.albumCoverUrl) {
            song.albumCoverUrl = 'assets/generic.jpeg';
          }
          if (!song.album) {
            song.album = 'Generic album';
          }
          if (!song.artist) {
            song.artist = 'Generic artist';
          }
          if (!song.title) {
            song.title = 'Generic title';
          }
        });
      })
    );
  }
}
