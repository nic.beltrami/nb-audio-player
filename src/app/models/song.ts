export class Song {
  title: string;
  artist: string;
  album: string;
  audioUrl: string;
  albumCoverUrl: string;
}
