import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AudioListComponent} from './audio-list.component';
import {AngularMaterialModule} from '../../angular-material.module';
import {By} from '@angular/platform-browser';
import {ResponsiveModule} from 'ngx-responsive';
import {Song} from '../../models/song';
import { MatSliderChange } from '@angular/material/slider';

describe('AudioListComponent', () => {
  let component: AudioListComponent;
  let fixture: ComponentFixture<AudioListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AudioListComponent],
      imports: [
        AngularMaterialModule,
        ResponsiveModule.forRoot()
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it(`should have a div 'header'`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    const header = fixture.debugElement.query(By.css('.header'));
    expect(header).toBeDefined();
  });

  it(`should have a div 'body'`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    const header = fixture.debugElement.query(By.css('.body'));
    expect(header).toBeDefined();
  });

  it(`should have an audio element`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    const audio = fixture.debugElement.query(By.css('audio'));
    expect(audio).toBeDefined();
  });

  it(`should have an number of header column 3`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    expect(component.displayedColumns.length).toBe(3);
  });

  it(`should set song`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    const song = new Song();
    song.title = 'test';
    component.setSong(song);
    expect(component.selectedSong).toBeDefined();
  });

  it(`should set song position`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    const event = new MatSliderChange();
    event.value = 100;
    component.setSongPosition(event);
    expect(component.audio.currentTime).toBe(100);
  });

  it(`should set song muted`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    const muted = component.audio.muted;
    component.setMute();
    expect(component.audio.muted).toBe(!muted);
  });

  it(`should set song volume`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    const event = new MatSliderChange();
    event.value = 1;
    component.setVolume(event);
    expect(component.audio.volume).toBe(1);
  });

  it(`should set songs shuffle`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    component.enableShuffle();
    expect(component.shuffle).toBe(true);
    expect(component.shuffleSongQueue.length).toBe(component.dataSource.length);
  });

  it(`should disable songs shuffle`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    component.disableShuffle();
    expect(component.shuffle).toBe(false);
    expect(component.shuffleSongQueue.length).toBe(0);
  });

  it(`should set next song`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    component.next();
    expect(component.selectedSongIndex).toBe(1);
  });

  it(`should set previous song`, () => {
    fixture = TestBed.createComponent(AudioListComponent);
    const song1 = new Song();
    const song2 = new Song();
    const song3 = new Song();
    component.dataSource = [song1, song2, song3];
    component.previous();
    expect(component.selectedSongIndex).toBe(2);
  });

});
