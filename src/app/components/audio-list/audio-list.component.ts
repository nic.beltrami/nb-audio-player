import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AudioService} from '../../services/audio.service';
import {Song} from '../../models/song';
import {finalize, first, takeUntil, tap} from 'rxjs/operators';
import { Observable, Subject, timer } from 'rxjs';
import * as moment from 'moment';
import {Moment} from 'moment';
import {MatSliderChange} from '@angular/material';
import { DeviceInfoRx } from "ngx-responsive";

@Component({
  selector: 'app-audio-list',
  templateUrl: './audio-list.component.html',
  styleUrls: ['./audio-list.component.scss']
})
export class AudioListComponent implements AfterViewInit, OnInit, OnDestroy {

  private destroySubject: Subject<void> = new Subject<void>();

  // TODO Implement is loading spinner
  public isLoading;

  // Table data
  public displayedColumns: string[];
  public dataSource: Song[];

  // Audio datas
  public volume = 1.0;
  public shuffle = false;
  private _shuffleSongQueue: Song[];
  private readonly CROSS_FATE_SECONDS: number = 8;
  private playing: boolean;

  // Selected Song
  public selectedSong: Song;

  // Time Data
  public currentTimeDate: Moment;
  public currentTimeDateInSeconds: number;
  public durationTimeDate: Moment;
  public durationTimeDateInSeconds: number;

  private _selectedSongIndex: number;

  private _audio;

  private deviceType: string;

  constructor(
    private readonly audioService: AudioService,
    private readonly devicesInfoRx: DeviceInfoRx
  ) {
    this._audio = new Audio();
  }

  ngOnInit(): void {

    this.devicesInfoRx.connect().pipe(
      tap((deviceType) => this.deviceType = deviceType)
    ).subscribe();

    // Inizialize table headers
    this.displayedColumns = ['title', 'artist', 'album'];

    // Inizialize shuffle Queue: When shuffle is enable read songs by this queue
    this._shuffleSongQueue = [];

    // Get list of songs
    // TODO Implement error when list not found
    this.isLoading = true;
    this.audioService.findAll().pipe(
      tap((songs) => {
        this.dataSource = songs;
        this.selectedSong = songs[0];
        this._selectedSongIndex = 0;
        this._audio.controls = true;
        this._audio.src = this.selectedSong.audioUrl;
        this._audio.load();
      }),
      finalize(() => this.isLoading = false)
    ).subscribe();
  }

  private isMobileIOS(): boolean {
    if (this.deviceType === 'ipad' || this.deviceType === 'iphone') {
      return true;
    }
    return false;
  }

  /**
   * Getter
   */

  public get shuffleSongQueue() {
    return this._shuffleSongQueue;
  }

  public get selectedSongIndex() {
    return this._selectedSongIndex;
  }

  public get audio() {
    return this._audio;
  }

  public ngAfterViewInit() {
    // Init audio element
    this._audio.addEventListener('loadeddata', (event) => {
      // Init time for progress bar
      this.currentTimeDate = moment(new Date()).startOf('day').seconds(0);
      this.currentTimeDateInSeconds = this.currentTimeDate.seconds();
      this.durationTimeDate = moment(new Date()).startOf('day').seconds(this._audio.duration);
      this.durationTimeDateInSeconds = this.durationTimeDate.diff(this.currentTimeDate.startOf('minutes'), 'seconds');
    });

    this._audio.volume = 0;
    this._audio.autoplay = false;
  }

  /**
   * Get time string format HH:ss
   */
  public getTime(date: Moment): string {
    return date ? date.format('mm:ss') : '';
  }

  /**
   * When click on table set selected song
   */
  public setSong(song: Song) {
    this.pause();
    this.selectedSong = song;
    this._audio.src = this.selectedSong.audioUrl;
    this._audio.load();
  }

  /**
   * Set pause
   */
  public pause(): void {
    this._audio.pause();
    this.playing = false;
    this.destroySubject.next();
  }

  /**
   * Getter to check if the audio is in pause
   */
  public get paused(): boolean {
    return this._audio ? this._audio.paused : true;
  }

  /**
   * Play audio file
   */
  public play(): void {
    if (this._audio.readyState >= 2) {

      this.audio.play();

      if (this.shuffle) {
        this._audio.autoplay = true;
      }

      this.playTimer().subscribe();
    }
  }

  private playTimer(): Observable<number> {
    return timer(1000, this._audio.duration).pipe(
      tap(() => {
        this.currentTimeDate = moment(new Date()).startOf('day').seconds(this._audio.currentTime);
        this.currentTimeDateInSeconds = this._audio.currentTime;

        const diffInSeconds = moment.duration(this.durationTimeDate.diff(this.currentTimeDate)).asSeconds();

        if ((this.durationTimeDateInSeconds - diffInSeconds) < this.CROSS_FATE_SECONDS) {
          this.crossFadeIn(diffInSeconds);
        } else if (diffInSeconds <= this.CROSS_FATE_SECONDS) {
          this.crossFadeOut(diffInSeconds);
        } else {
          this._audio.volume = this.volume;
        }
        if (this.shuffle && !this.isMobileIOS()) {
          if (diffInSeconds === 1) {
            const index = Math.floor(Math.random() * this._shuffleSongQueue.length);
            const nextSong = this._shuffleSongQueue[index];
            this._shuffleSongQueue.splice(index, 1);
            this.pause();
            this.selectedSong = nextSong;
            this._audio.src = nextSong.audioUrl;
            this._audio.load();
          }
        }
      })
    );
  }

  public setSongPosition(event: MatSliderChange) {
    this._audio.currentTime = event.value;
  }

  /**
   * Set mute state
   */
  public setMute(): void {
    this._audio.muted = !this._audio.muted;
  }

  /**
   * Get if volume is muted
   */
  public get muted(): boolean {
    return this._audio ? this._audio.muted : false;
  }

  /**
   * Set volume value
   */
  public setVolume(matSliderChange: MatSliderChange) {
    if (!this.isMobileIOS()) {
      this.volume = matSliderChange.value;
      this._audio.volume = matSliderChange.value;
    }
  }

  /**
   * Set next song
   */
  public next() {
    this.pause();
    const index = ++this._selectedSongIndex;
    this._selectedSongIndex = index % this.dataSource.length;
    this.selectedSong = this.dataSource[this._selectedSongIndex];
    this._audio.src = this.selectedSong.audioUrl;
    this._audio.load();
  }

  /**
   * Set previous song
   */
  public previous() {
    this.pause();
    const index = --this._selectedSongIndex;
    this._selectedSongIndex = (index < 0) ? (this.dataSource.length - Math.abs(index)) : index % this.dataSource.length;
    this.selectedSong = this.dataSource[this._selectedSongIndex];
    this._audio.src = this.selectedSong.audioUrl;
    this._audio.load();
  }

  private crossFadeOut(seconds: number) {
    this._audio.volume = (this.volume / (this.CROSS_FATE_SECONDS)) * seconds;
  }

  private crossFadeIn(seconds: number) {
    const volume = (1 / this.CROSS_FATE_SECONDS) * (this.durationTimeDateInSeconds - seconds);
    if (volume < this.volume) {
      this._audio.volume = volume;
    }
  }

  public enableShuffle() {
    this.shuffle = true;
    Object.assign(this._shuffleSongQueue, this.dataSource);
  }

  public disableShuffle() {
    this.shuffle = false;
    this._audio.autoplay = false;
    this._shuffleSongQueue = [];
  }

  ngOnDestroy(): void {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

}
