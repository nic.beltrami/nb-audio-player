import {NgModule} from '@angular/core';
import {MatCardModule, MatIconModule, MatSliderModule, MatTableModule} from '@angular/material';

@NgModule({
  imports: [
    MatCardModule,
    MatTableModule,
    MatIconModule,
    MatSliderModule
  ],
  declarations: [],
  providers: [],
  exports: [
    MatCardModule,
    MatTableModule,
    MatIconModule,
    MatSliderModule
  ]
})
export class AngularMaterialModule {
}
