import { AppPage } from './app.po';
import {browser, by, element, ExpectedConditions, logging, protractor} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should mute and after unmute', () => {
    page.navigateTo();
    const muteBtn = element(by.css('#mute_btn'));
    muteBtn.click();
    expect(element(by.xpath('//mat-icon[contains(text(),"volume_off")]')).isPresent()).toBeTruthy();
    muteBtn.click();
    expect(element(by.xpath('//mat-icon[contains(text(),"volume_up")]')).isPresent()).toBeTruthy();
  });

  it('should play about 3 seconds', () => {
    page.navigateTo();
    const playBtn = element(by.css('#play_btn'));
    playBtn.click();
    browser.wait(delayPromise(6000).then(() => {}), 10000);
  });

  function delayPromise(duration) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve();
        }, duration);
      });
  }

  it('should skip next 3 times and behind 2 times', () => {
    page.navigateTo();
    const songList = [];
    const nextBtn = element(by.css('#next_btn'));
    const previousBtn = element(by.css('#previous_btn'));
    const songTitle = element(by.css('#songTitle'));

    songTitle.getText().then((title) => {
      songList.push(title);
      nextBtn.click();
    });
    songTitle.getText().then((title) => {
      songList.push(title);
      nextBtn.click();
    });
    songTitle.getText().then((title) => {
      songList.push(title);
      nextBtn.click();
    });
    previousBtn.click();
    songTitle.getText().then((title) => {
      expect(songList[2]).toEqual(title);
    });
    previousBtn.click();
    songTitle.getText().then((title) => {
      expect(songList[1]).toEqual(title);
    });
 });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
