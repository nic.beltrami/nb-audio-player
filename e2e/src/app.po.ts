import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/index.html');
  }

  getTitleText() {
    return element(by.css('app-root .content span')).getText() as Promise<string>;
  }
}
